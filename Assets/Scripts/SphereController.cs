﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SphereController : MonoBehaviour {

	Gps gps;
	GameObject sphere;
	float ORIGIN_LATITUDE = 14.640059f;
	float ORIGIN_LONGITUDE = 121.078610f;
	float CONVERSION_FACTOR = 0.00006675f;

	// Use this for initialization
	void Start () {
		gps = GameObject.Find ("GPS System").GetComponent<Gps> ();
		sphere = GameObject.Find ("Sphere");

		gps.StartGps ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.location.status == LocationServiceStatus.Running) {

			float latit = gps.Latitude - ORIGIN_LATITUDE;
			float longi = gps.Longitude - ORIGIN_LONGITUDE;

			latit /= CONVERSION_FACTOR;
			longi /= CONVERSION_FACTOR;

			// sphere.transform.position = new Vector3(longi, 0.0f, latit);
		}
	}
}
