﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour {
	
	private float MAX_MAGNITUDE = 15f;		//Max speed for zooming out
	private float MIN_MAGNITUDE = -15f;		//Max speed for zooming in

	private float Y_AXIS_MAX_ZOOM = 50f;	//Max zoom out location of camera on y axis
	private float Y_AXIS_MIN_ZOOM = 15f;	//Max zoom in location of camera on y axis

	private float Z_AXIS_MAX_ZOOM = -15f;	//Max zoom out location of camera on z axis
	private float Z_AXIS_MIN_ZOOM = -50f;	//Max zoom in location of camera on z axis

	private float MAGNITUDE_THRESHOLD = 1f;	//Dictates the threshold for pinch zooming

	private float zoomSpeed = 0.5f;			//Camera zoom speed

	private Vector3 offset;					//Offset of the camera relative to the player object

	public GameObject player;
	private Text log; //Used for debugging, remove in engine

	void Start () {
		player = GameObject.Find ("Marker");
		offset = transform.position - player.transform.position;
		log = GameObject.Find ("Longitude").GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = player.transform.position + offset;

		if (Input.touchCount == 2) {
			Touch touchZero = Input.GetTouch (0);
			Touch touchOne = Input.GetTouch (1);

			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            if (deltaMagnitudeDiff > MAX_MAGNITUDE) deltaMagnitudeDiff = MAX_MAGNITUDE;
            if (deltaMagnitudeDiff < MIN_MAGNITUDE) deltaMagnitudeDiff = MIN_MAGNITUDE;

            Vector3 currentPosition = transform.position;

            // If the delta is not within the range (-MAGNITUDE_THRESHOLD..MAGNITUDE_THRESHOLD)
            if (!(deltaMagnitudeDiff < MAGNITUDE_THRESHOLD && deltaMagnitudeDiff > - MAGNITUDE_THRESHOLD)) {

            	offset = new Vector3(
            		offset.x,
            		Mathf.Clamp(offset.y + deltaMagnitudeDiff * zoomSpeed, Y_AXIS_MIN_ZOOM, Y_AXIS_MAX_ZOOM),
            		Mathf.Clamp(offset.z + deltaMagnitudeDiff * zoomSpeed * -1, Z_AXIS_MIN_ZOOM, Z_AXIS_MAX_ZOOM)
            	);

            }

            log.text = offset.y.ToString() + "\n" + offset.z.ToString() + "\n" + deltaMagnitudeDiff.ToString();
		}
	}
}
