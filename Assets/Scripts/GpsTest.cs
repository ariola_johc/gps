﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GpsTest : MonoBehaviour {

	public float latitude = 0;
	public float longitude = 0;
	private bool finishedInitializing = false;

	public bool IsFinishedInitializing {
		get {
			return finishedInitializing;
		}
	}

	public float Latitude {
		get {
			return latitude;
		}
	}

	public float Longitude {
		get {
			return longitude;
		}
	}

	private IEnumerator StartLocationService () {
		if (!Input.location.isEnabledByUser) {
			Debug.Log ("User has not enabled GPS");
			yield break;
		}

		Input.location.Start (20f, 0.1f);
		int maxWait = 20;

		while (Input.location.status == LocationServiceStatus.Initializing) {
			yield return new WaitForSeconds (1);
			maxWait--;
		}

		if (maxWait <= 0) {
			Debug.Log ("Timeout");
			yield break;
		}

		if (Input.location.status == LocationServiceStatus.Failed) {
			Debug.Log ("Unable to determine device location");
			yield break;
		}

		finishedInitializing = true;
	}

	void Start () {
		StartCoroutine (StartLocationService ());
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (finishedInitializing) {
			longitude = Input.location.lastData.longitude;
			latitude = Input.location.lastData.latitude;
		}
	}
}
