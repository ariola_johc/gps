﻿using UnityEngine;
using System.Collections;

public class Gps : MonoBehaviour 
{
    public float updateDistance = 0.1f;
    public float desiredAccuracy = 5.0f;
	public bool isPaused = true;

	private static float latitude = 0f;
	private static float longitude = 0f;
	private bool timedOut = false;
	private bool isWaitingForLocation;
    private int failureRetries = 0;

	private float countDown;
	private double prevTimestamp;
	
	public float Latitude
	{
		get
		{
			return latitude;
		}
	}

	public float Longitude
	{
		get
		{
			return longitude;
		}
	}

	public LocationServiceStatus Status
	{
		get
		{
			return Input.location.status;
		}
	}

	public bool IsRunning
	{
		get
		{
			return Input.location.status == LocationServiceStatus.Running;
		}
	}

	public float TrueHeading
	{
		get
		{
			return Input.compass.trueHeading;
		}
	}

	public bool HasTimedOut
	{
		get
		{
			return timedOut;
		}
	}

	public bool Started
	{
		get
		{
			return Input.location.status != LocationServiceStatus.Stopped;
		}
	}

	public bool IsWaitingForLocation
	{
		get
		{
			return isWaitingForLocation;
		}
	}


    void Start () 
	{
		prevTimestamp = 0.0;
    }
	
    void Update () 
	{
		if(!Started)
		{
			return;
		}

        if (Status == LocationServiceStatus.Failed)
        {
			if(failureRetries < 3)
			{
            	Input.location.Stop();
            	Input.location.Start(desiredAccuracy, updateDistance);
            	failureRetries++;
			}
        }

		if(!IsRunning) 
		{
			UpdateCountdown ();
            return;
        }

		failureRetries = 0;
		isWaitingForLocation = false;

		if(Input.location.lastData.timestamp != prevTimestamp)
		{
			timedOut = false;
            countDown = (float) ((System.DateTime.UtcNow - new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds
                                 - Input.location.lastData.timestamp);
            Debug.Log("GPS update! Countdown reset to " + countDown);
		}
        prevTimestamp = Input.location.lastData.timestamp;
        UpdateCountdown();

		latitude = Input.location.lastData.latitude;
		longitude = Input.location.lastData.longitude;
    }

	public void StartGps()
	{
		Input.location.Stop ();
        Input.location.Start(desiredAccuracy, updateDistance);
        Input.compass.enabled = false;
        Input.compass.enabled = true;

		countDown = 0f;
		isWaitingForLocation = true;
    }

    private void OnDestroy() 
	{
		timedOut = false;
		countDown = 0f;
        failureRetries = 0;
        Input.location.Stop();
        Input.compass.enabled = false;
    }


    private void OnApplicationPause(bool paused) 
	{
		isPaused = paused;
		

		if (paused) {
			OnDestroy ();
		} else {
			Start ();
			StartGps ();
		}
    }

	private void UpdateCountdown()
	{
		if(countDown >= 30f)
		{
			timedOut = true;
		}
		else
		{
			countDown += Time.deltaTime;
		}
	}
}
