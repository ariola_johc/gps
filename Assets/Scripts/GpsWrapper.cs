﻿using UnityEngine;
using System.Collections;

public class GpsWrapper : MonoBehaviour
{
	private Gps standardGps;

	private bool useDummy;
	private bool timedOut;

	private float dummyLatitude;
	private float dummyLongitude;

	public float Latitude
	{
		get
		{
			if(useDummy)
			{
				return dummyLatitude;
			}
			else
			{
				return standardGps.Latitude;
			}
		}
	}

	public float Longitude
	{
		get
		{
			if(useDummy)
			{
				return dummyLongitude;
			}
			else
			{
				return standardGps.Longitude;
			}
		}
	}

	public float TrueHeading
	{
		get
		{
			return Input.compass.trueHeading;
		}
	}

	public bool Started
	{
		get
		{
			if(useDummy)
			{
				return true;
			}
			else
			{
				return standardGps.Started;
			}
		}
	}

	public bool HasTimedOut
	{
		get
		{
			if(useDummy)
			{
				return timedOut;
			}
			else
			{
				return standardGps.HasTimedOut;
			}
		}
	}

	public bool IsWaitingForLocation
	{
		get
		{
			if(useDummy)
			{
				return (dummyLatitude == 0f) && (dummyLongitude == 0f);
			}
			else
			{
				return standardGps.IsWaitingForLocation;
			}
		}
	}

	public void StartGps()
	{
		if(!useDummy)
		{
			standardGps.StartGps ();
		}
	}

	public void UseDummy(bool useDummy)
	{
		this.useDummy = useDummy;
	}

	public bool IsDummy()
	{
		return useDummy;
	}

	public void ToggleUseDummy()
	{
		UseDummy (!useDummy);

		if(useDummy)
		{
			Debug.Log ("Using dummy GPS");
		}
		else
		{
			Debug.Log ("Using standard GPS");
		}
	}

	public void SetDummyLocation(float longitude, float latitude)
	{
		dummyLatitude = latitude;
		dummyLongitude = longitude;
	}

	public void SetTimedOut(bool timedOut)
	{
		if(useDummy)
		{
			this.timedOut = timedOut;
		}
	}

	void Start()
	{
		if(GameObject.Find ("GpsSystem") != null)
		{
			standardGps = GameObject.Find ("GpsSystem").GetComponent<Gps>();
		}

		useDummy = false;
	}
}
