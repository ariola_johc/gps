﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour {

	private Text latitude;
	private Text longitude;
	private Gps gps;

	public float smooth = 2.0F;

	void Start () {
		gps = GameObject.Find ("GPS System").GetComponent<Gps> ();
		gps.StartGps ();
		//gps = GameObject.Find ("GPS").GetComponent<GpsTest> ();
		latitude = GameObject.Find ("Latitude").GetComponent<Text> ();
		longitude = GameObject.Find ("Longitude").GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//longitude.text = Input.location.status.ToString () + "\n" + gps.IsWaitingForLocation.ToString () + "\n" + gps.TrueHeading + "\n" + gps.isPaused;

		if (Input.location.status == LocationServiceStatus.Running) {
			Int32 timestamp = (Int32)(System.DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
			latitude.text = (gps.Latitude).ToString ("R") + ", " +
				(gps.Longitude).ToString ("R") + "\n" +
				(timestamp - ((int)(Input.location.lastData.timestamp))).ToString();
			Debug.Log ("latitude: " + (gps.Latitude).ToString () + ", longitude: " + (gps.Longitude).ToString ());
		}
	}
}
