﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarkerController : MonoBehaviour {
	
	GameObject deviceLocation;
	Gps gps;
	float step;

	float ORIGIN_LATITUDE = 14.640059f;
	float ORIGIN_LONGITUDE = 121.078610f;
	float CONVERSION_FACTOR = 0.00006675f;
	float rotationSpeed = 20f;

	Text log;	//for debugging purposes

	void Start () {
		deviceLocation = GameObject.Find ("Sphere");
		step = 15 * Time.deltaTime;
		gps = GameObject.Find ("GPS System").GetComponent<Gps> ();
		log = GameObject.Find ("Latitude").GetComponent<Text> ();
		gps.StartGps ();
	}
	

	void Update () {
		Quaternion targetHeading = Quaternion.Euler (0, gps.TrueHeading, 0);	//gets compass data to use for object orientation
		Vector3 targetLocation;	//current device location on unity world units


		if (Input.location.status == LocationServiceStatus.Running) {

			//Offsetting
			float latit = gps.Latitude - ORIGIN_LATITUDE;
			float longi = gps.Longitude - ORIGIN_LONGITUDE;

			//Scaling down the GPS information
			latit /= CONVERSION_FACTOR;
			longi /= CONVERSION_FACTOR;

			targetLocation = new Vector3(longi, 0.0f, latit);
			transform.position = Vector3.MoveTowards (transform.position, targetLocation, step);
		}

		transform.rotation = Quaternion.Slerp (transform.rotation, targetHeading, rotationSpeed * Time.deltaTime);
	}
}
